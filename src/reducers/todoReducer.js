import { GET_ALL_SUCCESS, GET_ALL_LOADING, GET_ALL_FAIL,
         UPDATE_TODO_SUCCESS, UPDATE_TODO_LOADING, UPDATE_TODO_FAIL,
         NEW_TODO_LOADING, NEW_TODO_SUCCESS, NEW_TODO_FAIL } from '../actions/types';
import axios from 'axios';

const initialState = {
  todos: [],
  todo: {},

  getAllLoading: undefined,
  getAllSuccess: undefined,
  getAllFail: undefined,

  updateTodoLoading: undefined,
  updateTodoSuccess: undefined,
  updateTodoFail: undefined,

  newTodoLoading: undefined,
  newTodoSuccess: undefined,
  newTodoFail: undefined
};

const todoReducer = (state = initialState, action) => {
  console.log('todoReducer', action);
    switch(action.type) {
        case GET_ALL_LOADING:
            return {...state, getAllSuccess: undefined, getAllFail: undefined, getAllLoading: true};
        case GET_ALL_SUCCESS:
            return {...state, todos: action.receivedTodos, getAllSuccess: true, getAllFail: undefined, getAllLoading: undefined};
        case UPDATE_TODO_LOADING:
            return {...state, updateTodoSuccess: undefined, updateTodoFail: undefined, updateTodoLoading: true};
        case UPDATE_TODO_SUCCESS:
            return {...state, todos: updateTodoItem(state.todos, action.todoItem), updateTodoSuccess: true, updateTodoFail: undefined, updateTodoLoading: undefined};
        case NEW_TODO_LOADING:
            return {...state, newTodoSuccess: undefined, newTodoFail: undefined, newTodoLoading: true};
        case NEW_TODO_SUCCESS:
            return {...state, todos: addTodoItem(state.todos, action.todoItem), newTodoSuccess: true, newTodoFail: undefined, newTodoLoading: undefined};
        default:
            return state;
    }
}

updateTodoItem = (todos, todoItem) => {
  return todos.map((item) => {
    if(item.id == todoItem.id) {
      return todoItem;
    }
    return item;
  });
};

addTodoItem = (todos, todoItem) => {
  todoItem.id = todos.length + 1;
  todos.unshift(todoItem);
  return todos;
};

export default todoReducer;
