import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input, Button } from "react-native-elements";
import { View } from "react-native";

export default class TodoAdd extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      userId: 1,
      completed: false
    }

    this.onAddTodo = this.onAddTodo.bind(this);
  }

  onAddTodo() {
    this.setState({title: ''});
    this.props.onAddTodo(this.state);
  }

  render() {
    return (
      <View style={{ flexDirection: "column", padding: 20 }}>
        <Input
          placeholder="Add a task..."
          onChangeText={title => this.setState({title})}
          value={this.state.title}
          rightIcon={
            <Button
              icon={<Icon name="check-circle" size={15} color="white" />}
              onPress={this.onAddTodo}
              iconRight
            />
          }
        />
      </View>
    );
  }
}
