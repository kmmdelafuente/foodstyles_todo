import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { Button, Input, Overlay } from "react-native-elements";
import { View } from "react-native";

export default class TodoEditItem extends React.Component {
  constructor(props) {
    super(props);

    const { isVisible } = this.props;
    this.state = {
      isVisible: isVisible
    };
    console.log('constructor', isVisible, this.state);

    //this.onAddTodo = this.onAddTodo.bind(this);
  }

  handleChange(event) {
    console.log('handleChange', event)
  }

  onAddTodo= () => {

  };

  render() {
    const buttons = (
      <View style={{ flexDirection: "row" }}>
        <Button
          style={{margin: 5}}
          icon={<Icon name="check-circle" size={15} color="white" />}
          onPress={this.onAddTodo}
          iconRight
        />
        <Button
          style={{margin: 5}}
          icon={<Icon name="times-circle" size={15} color="white" />}
          onPress={this.onAddTodo}
          iconRight
        />
      </View>
    );

    console.log(this.state);

    return (
      <Overlay onBackdropPress={() => this.setState({ isVisible: false })} isVisible={this.props.isVisible}>
        <View>
          <Input
            placeholder="Add a task..."
            onChangeText={title => this.setState({ title })}
            rightIcon={
              <Button icon={buttons} onPress={this.onAddTodo} iconRight />
            }
          />
        </View>
      </Overlay>
    );
  }
}
