import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Header } from "react-native-elements";
import TodoAdd from "./TodoAdd";
import TodoItems from "./TodoItems";

export default class TodoAll extends React.Component {
  constructor(props) {
    super(props);

    this.onToggleCompleted = this.onToggleCompleted.bind(this);
    this.onAddTodo = this.onAddTodo.bind(this);
  }

  onToggleCompleted(todoItem) {
    this.props.onToggleCompleted(todoItem);
  }

  onAddTodo(todoItem) {
    this.props.onAddTodo(todoItem);
  }

  render() {
    return (
      <View style={styles.container}>
        <Header centerComponent={{ text: "Todos", style: { color: "#fff" } }} />
        <TodoAdd onAddTodo={this.onAddTodo} />
        <TodoItems todos={this.props.todos} isListLoading={this.props.isListLoading} onToggleCompleted={this.onToggleCompleted} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignSelf: "stretch"
  }
});
