import React from "react";
import { ActivityIndicator, StyleSheet, ScrollView, View } from "react-native";

import TodoItem from "./TodoItem";
import TodoEditItem from "./TodoEditItem";

export default class TodoItems extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showItemMenu: false,
      itemMenuTodo: undefined
    };

    this.onToggleCompleted = this.onToggleCompleted.bind(this);
    this.onShowItemMenu = this.onShowItemMenu.bind(this);
  }

  onToggleCompleted(todoItem) {
    this.props.onToggleCompleted(todoItem);
  }

  onShowItemMenu = todoItem => {
    this.setState({ showItemMenu: true, itemMenuTodo: todoItem });
  }

  render() {
    let todoItems;
    if (this.props.todos) {
      todoItems = this.props.todos.map((todo, index) => {
        return (
          <TodoItem
            key={todo.id}
            todoItem={todo}
            onShowItemMenu={this.onShowItemMenu}
            onToggleCompleted={this.onToggleCompleted}
          />
        );
      });
    }

    console.log(this.state);

    const { isListLoading } = this.props;
    const loadingIndicator = (
      <View
        style={{
          flex: 1,
          justifyContent: "center"
        }}
      >
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
    const scrollList = (
      <ScrollView style={styles.todoListContainer}>
        <TodoEditItem
          todoItem={this.state.itemMenuTodo}
          isVisible={this.state.showItemMenu}
        />
        <View>{todoItems}</View>
      </ScrollView>
    );
    const currentView = isListLoading ? loadingIndicator : scrollList;

    return currentView;
  }
}

const styles = StyleSheet.create({
  todoListContainer: {
    padding: 20,
    flex: 2
  }
});
