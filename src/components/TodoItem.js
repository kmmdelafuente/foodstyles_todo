import React from "react";
import { CheckBox } from "react-native-elements";

export default class TodoItem extends React.Component {
  constructor(props) {
    super(props);

    this.onToggleCompleted = this.onToggleCompleted.bind(this);
    this.onShowItemMenu = this.onShowItemMenu.bind(this);
  }

  onToggleCompleted(todoItem) {
    this.props.onToggleCompleted(todoItem);
  }

  onShowItemMenu(todoItem) {
    console.log('long press!', todoItem);
    this.props.onShowItemMenu(todoItem);
  }

  render() {
    return (
      <CheckBox
        checked={this.props.todoItem.completed}
        title={this.props.todoItem.title}
        onPress={() => this.onToggleCompleted(this.props.todoItem)}
        onLongPress={() => this.onShowItemMenu(this.props.todoItem)}
      />
    );
  }
}
