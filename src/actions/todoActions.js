import { ALL_TODOS, GET_ALL_LOADING, GET_ALL_SUCCESS, GET_ALL_FAIL,
         UPDATE_TODO, UPDATE_TODO_LOADING, UPDATE_TODO_SUCCESS, UPDATE_TODO_FAIL,
         NEW_TODO, NEW_TODO_LOADING, NEW_TODO_SUCCESS, NEW_TODO_FAIL} from './types';

export const getAllTodos = () => {
    return {
        type: ALL_TODOS
    };
}

export const getAllLoading = () => {
    return {
        type: GET_ALL_LOADING
    }
}

export const getAllSuccess = (receivedTodos) => {
    return {
        type: GET_ALL_SUCCESS,
        receivedTodos
    }
}

export const getAllFail = (error) => {
    return {
        type: GET_ALL_FAIL,
        error
    }
}

export const updateTodo = (todoItem) => {
    return {
        type: UPDATE_TODO,
        todoItem
    };
}

export const updateTodoLoading = () => {
    return {
        type: UPDATE_TODO_LOADING
    }
}

export const updateTodoSuccess = (receivedTodos) => {
    return {
        type: UPDATE_TODO_SUCCESS,
        receivedTodos
    }
}

export const updateTodoFail = (error) => {
    return {
        type: UPDATE_TODO_FAIL,
        error
    }
}

export const addTodo = (newTodo) => {
    return {
        type: NEW_TODO,
        newTodo
    };
}

export const addTodoLoading = () => {
    return {
        type: NEW_TODO_LOADING
    }
}

export const addTodoSuccess = (todoItem) => {
    return {
        type: NEW_TODO_SUCCESS,
        todoItem
    }
}

export const addTodoFail = (error) => {
    return {
        type: NEW_TODO_FAIL,
        error
    }
}
