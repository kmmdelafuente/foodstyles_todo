export const ALL_TODOS = 'ALL_TODOS';
export const UPDATE_TODO = 'UPDATE_TODO';
export const ADD_TODO = 'ADD_TODO';
export const NEW_TODO = 'NEW_TODO';

// ALL_TODOS status messages
export const GET_ALL_LOADING = 'GET_ALL_LOADING';
export const GET_ALL_SUCCESS = 'GET_ALL_SUCCESS';
export const GET_ALL_FAIL = 'GET_ALL_FAIL';

// UPDATE_TODO status messages
export const UPDATE_TODO_LOADING = 'UPDATE_TODO_LOADING';
export const UPDATE_TODO_SUCCESS = 'UPDATE_TODO_SUCCESS';
export const UPDATE_TODO_FAIL = 'UPDATE_TODO_FAIL';

// NEW_TODO status messages
export const NEW_TODO_SUCCESS = 'NEW_TODO_SUCCESS';
export const NEW_TODO_FAIL = 'NEW_TODO_FAIL';
export const NEW_TODO_LOADING = 'NEW_TODO_LOADING';
