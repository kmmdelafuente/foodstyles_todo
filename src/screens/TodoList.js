/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button } from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";

import { getAllTodos, updateTodo, addTodo } from "../actions";

import TodoAll from "../components/TodoAll";

type Props = {};
class TodoList extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      showItemMenu: false
    }

    this.onToggleCompleted = this.onToggleCompleted.bind(this);
    this.onAddTodo = this.onAddTodo.bind(this);
  }

  componentWillMount() {
    console.disableYellowBox = true;
    this.props.onGetAllTodos();
  }

  onToggleCompleted(todoItem) {
    todoItem.completed = !todoItem.completed;
    this.props.onUpdateTodo(todoItem);
  }

  onAddTodo(todoItem) {
    if(todoItem) {
      this.props.onAddTodo(todoItem);
    }
  }

  render() {
    console.log(this.props.todos);
    return (
      <View style={styles.container}>
        <TodoAll
          todos={this.props.todos}
          onToggleCompleted={this.onToggleCompleted}
          onLoadMoreTodos={this.props.onLoadMoreTodos}
          onAddTodo={this.onAddTodo}
          isListLoading={this.props.getAllLoading}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = state => {
  return {
    todos: state.todoReducer.todos,
    todo: state.todoReducer.todo,

    getAllLoading: state.todoReducer.getAllLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetAllTodos: () => {
      dispatch(getAllTodos());
    },
      onUpdateTodo: todoItem => {
        dispatch(updateTodo(todoItem));
      },
    onAddTodo: newTodo => {
      dispatch(addTodo(newTodo));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
