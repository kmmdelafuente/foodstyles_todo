import axios from "axios";

const todosUrl = "https://jsonplaceholder.typicode.com/todos";
function* getTodos() {
  const response = yield axios
    .get(todosUrl, { responseType: "json" })
    .then(res => {
      return res;
    });
  const todos = yield response.status === 200 ? response.data : [];
  return todos;
}

function* updateTodo(todoItem) {
  const response = yield axios
    .get(`${todosUrl}/${todoItem.id}`, { responseType: "json" })
    .then(res => {
      return res;
    });
  const data = yield response.status === 200 ? response.data : [];
  return data;
}

function* insertNewTodo(newTodo) {
  const response = yield axios.get(todosUrl, newTodo).then(res => {
    return res;
  });
  console.log('response ' + response.status);
  return yield response.status === 201;
}

export const Api = {
  getTodos,
  updateTodo,
  insertNewTodo
};
