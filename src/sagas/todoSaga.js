import {
  ALL_TODOS,
  UPDATE_TODO,
  GET_ALL_LOADING,
  GET_ALL_SUCCESS,
  GET_ALL_FAIL,
  UPDATE_TODO_LOADING,
  UPDATE_TODO_SUCCESS,
  UPDATE_TODO_FAIL,
  NEW_TODO,
  NEW_TODO_LOADING,
  NEW_TODO_SUCCESS,
  NEW_TODO_FAIL
} from "../actions/types";

import { put, takeLatest } from "redux-saga/effects";
import { Navigation } from "react-native-navigation";

import { Api } from "./Api";

function* loadTodos() {
  try {
    yield put({ type: GET_ALL_LOADING });
    const receivedTodos = yield Api.getTodos();
    yield put({ type: GET_ALL_SUCCESS, receivedTodos });
  } catch (error) {
    yield put({ type: GET_ALL_FAIL, error });
  }
}

export function* watchListTodos() {
  yield takeLatest(ALL_TODOS, loadTodos);
}

function* updateTodo(action) {
  try {
    const todoItem = action.todoItem;
    yield put({ type: UPDATE_TODO_LOADING });
    const response = yield Api.updateTodo(todoItem);
    yield put({ type: UPDATE_TODO_SUCCESS, todoItem });
  } catch (error) {
    yield put({ type: UPDATE_TODO_FAIL, error });
  }
}

export function* watchUpdateTodo() {
  yield takeLatest(UPDATE_TODO, updateTodo);
}

function* addNewTodo(action) {
  try {
    const todoItem = action.newTodo;
    yield put({ type: NEW_TODO_LOADING });
    const result = yield Api.insertNewTodo(todoItem);
    yield put({ type: NEW_TODO_SUCCESS, todoItem });
  } catch (error) {
    yield put({ type: NEW_TODO_FAIL, error });
  }
}

export function* watchAddTodo() {
  yield takeLatest(NEW_TODO, addNewTodo);
}
