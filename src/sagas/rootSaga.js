import { fork } from "redux-saga/effects";

import {
  watchListTodos,
  watchAddTodo,
  watchUpdateTodo
} from "./todoSaga";

export default function* rootSaga() {
  yield fork(watchListTodos);
  yield fork(watchUpdateTodo);
  yield fork(watchAddTodo);
}
