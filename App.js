/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import allReducers from './src/reducers';

import TodoList from './src/screens/TodoList';

const sagaMiddleware = createSagaMiddleware();
import rootSaga from './src/sagas/rootSaga';

const store = createStore(allReducers, applyMiddleware(sagaMiddleware));

Navigation.registerComponentWithRedux('TodoList', () => TodoList, Provider, store);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: 'TodoList'
      }
    }
  });
});

sagaMiddleware.run(rootSaga);
